import java.util.Random;

public class TextGenerator {

    /**
     * Метод генерирует слово заданной длины
     * @param length
     * @return
     */
  public static String getWord(int length) {
      String abc = "abcdefghijklmnopqrstuvxyz";
      StringBuilder sb = new StringBuilder(length);
      for (int i = 0; i < length; i++) {
          int index = (int)(abc.length() * Math.random()); //берем случайными образом индекс элемента в строке abc
          sb.append(abc.charAt(index)); //добавляем наш элемент в StringBuffer, формируя новую строку
      }

      return sb.toString(); //возвращаем получившееся слово
  }
}
