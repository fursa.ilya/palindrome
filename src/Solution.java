import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        List<String> words = new ArrayList<>(); //список слов
        //вводим длину слова. Рекомендую 3 символа(Для теста хватит).Чем больше длина, тем больше слов нужно сгенерировать чтобы попался палиндром
        Scanner sc = new Scanner(System.in);
        System.out.print("Please enter word size: ");
        int size = sc.nextInt();
        System.out.println("Current word size = " + size);


        for (int i = 0; i < 1000; i++) { //Количество сгенерированных слов 1000, для большего количества палиндромов, лучше установить больше
           String word = TextGenerator.getWord(size); //Генерация слова заданной длины. size - длина слова
           words.add(word); //Добавляем слово в список
        }

        List<String> palindromes = Palindrome.findPalindromes(words); //проходимся по списку, формируем новый список включающий только слова палиндромы
        System.out.println("Number of palindromes found in current text: " + palindromes.size()); //выводим количество найденных палиндромов

        for (int i = 0; i < palindromes.size(); i++) { //Печатаем какие палиндромы были найдены в нашем снегерированном списке
            String palindrome = palindromes.get(i);
            System.out.println("Found " + palindrome);
        }

    }
}
