import java.util.ArrayList;
import java.util.List;

/**
 * Класс для нахождения палиндромов в заданном списке
 */
public class Palindrome {

    /**
     *
     * @param palindromes - сгенерированный с помощью TextGenerator список из слов заданной длины
     * @return возвращает список из палиндромов найденных в списке слов
     */
    public static List<String> findPalindromes(List<String> palindromes) {
        List<String> result = new ArrayList<>();

        for (int i = 0; i < palindromes.size(); i++) { //проходимся по списку
            String word = palindromes.get(i); //берем каждое слово
            String reversedWord = new StringBuilder(word).reverse().toString(); //переворачиваем слово

            if(word.equals(reversedWord)) { //сравниваем оригинал и перевернутое слово. Если они равны, то палиндром найден и записывается в список
                result.add(word);
            }
        }

        return result; //Возвращаем список
    }
}
